; Copyright 2009 Castle Technology Ltd
;
; Licensed under the Apache License, Version 2.0 (the "License");
; you may not use this file except in compliance with the License.
; You may obtain a copy of the License at
;
;     http://www.apache.org/licenses/LICENSE-2.0
;
; Unless required by applicable law or agreed to in writing, software
; distributed under the License is distributed on an "AS IS" BASIS,
; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
; See the License for the specific language governing permissions and
; limitations under the License.
;

ARMv3    *       0
ARMv4    *       1
ARMv4T   *       2
ARMv5    *       3
ARMv5T   *       4
ARMv5TE  *       5
ARMv5TEJ *       6
ARMv6    *       7
ARMvF    *       &F ; 'Fancy' ARM that describes its features in the feature registers (Cortex/ARMv7 and above?)

                ^       0
ARM600          #       1
ARM610          #       1
ARM700          #       1
ARM710          #       1
ARM710a         #       1
SA110_preRevT   #       1
SA110           #       1
ARM7500         #       1
ARM7500FE       #       1
SA1100          #       1
SA1110          #       1
ARM720T         #       1
ARM920T         #       1
ARM922T         #       1
X80200          #       1
X80321          #       1
Cortex_A8       #       1
ARMunk          *       255

; These flags are stored in ProcessorFlags and returned by OS_PlatformFeatures 0 (Read code features)

CPUFlag_SynchroniseCodeAreas    * 1:SHL:0       ; Calls to OS_SynchroniseCodeAreas required
CPUFlag_InterruptDelay          * 1:SHL:1       ; Clearing then setting I bit immediately doesn't trigger IRQs
CPUFlag_VectorReadException     * 1:SHL:2       ; 26-bit reads of hardware vectors abort
CPUFlag_StorePCplus8            * 1:SHL:3       ; Stores of R15 store PC+8 rather than PC+12
CPUFlag_BaseRestored            * 1:SHL:4       ; Base Restored abort model rather than Base Updated
CPUFlag_SplitCache              * 1:SHL:5       ; CPU has separate I and D caches
CPUFlag_32bitOS                 * 1:SHL:6       ; OS is 32-bit
CPUFlag_No26bitMode             * 1:SHL:7       ; CPU does not support 26-bit modes
CPUFlag_LongMul                 * 1:SHL:8       ; Has M extensions (UMULL etc)
CPUFlag_Thumb                   * 1:SHL:9       ; Supports Thumb
CPUFlag_DSP                     * 1:SHL:10      ; Has E extensions (QADD etc)
CPUFlag_ExtendedPages           * 1:SHL:15      ; Supports extended small page L2 descriptors
CPUFlag_NoWBDrain               * 1:SHL:16      ; CPU does not support Drain Write Buffer instruction
CPUFlag_AbortRestartBroken      * 1:SHL:17      ; Aborts do not correctly follow documented abort model
CPUFlag_XScale                  * 1:SHL:18      ; it's an XScale, so weird debug etc
CPUFlag_XScaleJTAGconnected     * 1:SHL:19      ; JTAG has been connected

; The macro to do an ARM operation. All ARM operations are expected
; to corrupt a1 only
; This macro corrupts ip unless $zero reg is supplied

        MACRO
        ARMop   $op, $cond, $tailcall, $zero
 [ "$zero" = ""
        MOV$cond ip, #ZeroPage
 ]
 [ "$tailcall" = ""
        MOV$cond lr, pc
 ]
 [ "$zero" = ""
        LDR$cond pc, [ip, #Proc_$op]
 |
        LDR$cond pc, [$zero, #Proc_$op]
 ]
        MEND

        MACRO
        ChangedProcVecs $tmp
 [ XScaleJTAGDebug
        MOV     $tmp, #0
        LDR     $tmp, [$tmp, #ProcessorFlags]
        TST     $tmp, #CPUFlag_XScaleJTAGconnected
        BEQ     %FT01
        BKPT    &2000
01
 ]
        MEND


        END
